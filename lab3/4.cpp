#include <stdio.h>
#include <conio.h>
int main()
{
	int family = 0, maxAge = 0, minAge = 150, i = 0;
	char name[50][50];
	int age[50];
	char *old, *young;
	printf("How many people in your family? \n");
	scanf("%d", &family);
	while (i < family) {
		printf("Enter name:\n");
		scanf("%s", &name[i]);
		printf("Enter age:\n");
		scanf("%d", &age[i]);
		if (age[i]>maxAge) {
			old = &name[i][0];
			maxAge = age[i];
		}
		else if (age[i] < minAge) {
			young = &name[i][0];
			minAge = age[i];
		};
		i++;
	};
	printf("%s is an oldest , she\\he is %d\n", old, maxAge);
	printf("%s is an youngest, she\\he is %d \n", young, minAge);
	return 0;
}