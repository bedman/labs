#include <stdio.h>

int main () {
    int max_len=10, count=0, i=0, digit=0, sum=0;
    char str[256];
    printf("Enter a string: ");
    fgets(str, 256, stdin);
    while (str[i]) {
        if (str[i]>='0' && str[i]<='9') {
            if (count < max_len) {
                digit=digit*10+(str[i]-'0');
                count++;
            }
            else {
                sum= sum+digit;
                count=0;
                digit=str[i]-'0';
            }
            
        }
        else {
            count=0;
            sum= sum+digit;
            digit=0;
        }
        i++;
    }
    printf("%d \n", sum);
    return 0;
}