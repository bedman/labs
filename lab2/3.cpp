#include <stdio.h>
#include <string.h>

int main () {
    int i=0;
	char str[256];
    printf("Enter a string: ");
    fgets(str,256,stdin);
    for (i=0;str[i]!='\0'; i++) {
        if (str[i]>='a' && str[i]<='z' || str[i]>='A' && str[i]<='Z') {
            printf("%c",str[i]);
        }
    }
    i=0;
    printf(" ");
    for (i=0;str[i]!='\0'; i++) {
        if (str[i]>='0' && str[i]<='9') {
            printf("%c",str[i]);
        }
    }
    printf("\n");
    return 0;
}