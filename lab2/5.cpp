#include <stdio.h>
 
#define XSIZ 6
#define YSIZ 6
 
int main()
{
    int i = 0;
    int j = 0;
    
    char shape[XSIZ][YSIZ] =  { "  *  ",
                                " * * ",
                                "*   *",
                                " * * ",
                                "  *  " 
    };
    
    for(i = 0; i < XSIZ; ++i) {
        for(j = 0; j < YSIZ; ++j)
            printf("%c", shape[i][j]);
        printf("\n");
    }
    
    return 0;
}